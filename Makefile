#	Encoding: UTF-8
#	Name: Makefile
#	Date: 29.Nov.2013
#	Copyright (C) 2013 Michal Riša <mh74s74@gmail.com>
#	                               <xrisam01@stud.fit.vutbr.cz>

#This file is part of 3d_fractal_raytracer.

#3d_fractal_raytracer is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License version 3 as
#published by the Free Software Foundation.

#3d_fractal_raytracer is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with 3d_fractal_raytracer. If not, see <http://www.gnu.org/licenses/>.

#vysledna binarka
out=tracer

#falosne ciele
.PHONY: all $(out) clean clean_ppm clean_png clean_gif clean_all run test png gif final git_clone git_push

all: $(out)

$(out): multicore.cpp
	g++ multicore.cpp -O3 -Wall -Wextra -lpthread -s -o $(out)

#cleanup vsetkeho druhu
clean:
	rm -f $(out) *.o

clean_ppm:
	rm -f *.ppm

clean_png:
	rm -f *.png

clean_gif:
	rm -f *.gif

clean_all: clean clean_ppm clean_png clean_gif

#renderovanie: ./tracer [threads] [width] [height]
run: $(out)
	./$(out) 2 100 100 anim.inc

#kontrola prace s pametou
test: $(out)
	valgrind --leak-check=full --show-reachable=yes ./$(out) 2 20 20 anim.inc

#prevod snimkov na *.png
png:
	bash make.sh png

#prevod snimkov na fractal.gif
gif:
	bash make.sh gif

#vytvori archiv odovzdavany do WIS-u
final:
	zip xrisam01.zip -r doc win multicore.cpp Makefile Makefile.win anim.inc make.sh copying fdl gpl pthreadGC2.dll

git_clone:
	git clone https://bitbucket.org/tibi225/pgr-projekt.git

git_push:
	git commit -a
	git push
