#!/bin/sh

#	Encoding: UTF-8
#	Name: Makefile
#	Date: 29.Nov.2013
#	Author: Michal Riša <xrisam01@stud.fit.vutbr.cz>
#	Copyright (C) 2013 Michal Riša <mh74s74@gmail.com>
#	                               <xrisam01@stud.fit.vutbr.cz>

#This file is part of 3d_fractal_raytracer.

#3d_fractal_raytracer is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License version 3 as
#published by the Free Software Foundation.

#3d_fractal_raytracer is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with 3d_fractal_raytracer. If not, see <http://www.gnu.org/licenses/>.

check_ppm() {
	#kontrola existenie snimkov
	files=$(ls *.ppm)
	if [ -z "$files" ]; then
		echo "Neexistujuce snimky - vygeneruj ich cez 'make run' ."
		exit 1
	fi
}

run_png() {
	check_ppm

	#prevedie snimky (*.ppm)do *.png
	for file in $files; do
		#odstrani koncovku .ppm
		file_noext=$(echo "$file" | cut -d. -f1)
		#prevediet x.ppm -> x.png
		convert "$file_noext.ppm" "$file_noext.png"
	done
}

function run_gif {
	check_ppm

	#spravi fractal.gif z *.ppm snimkov
	convert $files fractal.gif
}

if [ $# -ne 1 ]; then
	#spatne parametre
	echo "Pouzitie: sh make.sh [ciel={png/gif}]"
	exit 1
fi

if [ "$1" = "png" ]; then
	run_png
elif [ "$1" = "gif" ]; then
	run_gif
else
	echo "Neznamy ciel '$1'"
	exit 1
fi

exit $?
