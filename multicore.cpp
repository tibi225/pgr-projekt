/*
	A very basic raytracer example.
	Copyright (C) 2012	www.scratchapixel.com
	Copyright (C) 2013 Michal Riša <mh74s74@gmail.com>
									 <xrisam01@stud.fit.vutbr.cz>

	This file is part of 3d_fractal_raytracer.

	3d_fractal_raytracer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	3d_fractal_raytracer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with 3d_fractal_raytracer. If not, see <http://www.gnu.org/licenses/>.

	- changes 02/04/13: fixed flag in ofstream causing a bug under Windows,
	added default values for M_PI and INFINITY
	- changes 24/05/13: small change to way we compute the refraction direction
	vector (eta=ior if we are inside and 1/ior if we are outside the sphere)

	- changes 29/Nov/2013: edited to render 3D fractal.

	Compile with the following command: make
*/

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <fstream>
#include <vector>
#include <iostream>
#include <cassert>

#ifdef __linux__
	// "Compiled for Linux
#else
	// Windows doesn't define these values by default, Linux does
# ifndef M_PI
#	define M_PI 3.141592653589793
# endif
# ifndef INFINITY
#	define INFINITY 1e8
# endif
#endif

// 4 zlozkovy vektor (4 zlozka reprezentuje vahu - w)
template < typename T > class Vec4 {
public:
	T x, y, z, w;
	Vec4():x(T(0)), y(T(0)), z(T(0)), w(T(0)) {

	}
	Vec4(T xx):x(xx), y(xx), z(xx), w(xx) {

	}
	Vec4(T xx, T yy, T zz, T ww):x(xx), y(yy), z(zz), w(ww) {

	}
	Vec4(const Vec4 < T > &v) : x(v.x), y(v.y), z(v.z), w(v.w) {

	}
	inline Vec4 < T > &operator *=(const Vec4 < T > &v) {
		x *= v.x, y *= v.y, z *= v.z, w *= v.w;
		return *this;
	}
	inline Vec4 < T > operator *(const T & f)const {
		return Vec4 < T > (x * f, y * f, z * f, w * f);
	}
	inline Vec4 < T > operator *(const Vec4 < T > &v)const {
		return Vec4 < T > (x * v.x, y * v.y, z * v.z, w * v.w);
	}
	inline Vec4 < T > operator +(const Vec4 < T > &v)const {
		return Vec4 < T > (x + v.x, y + v.y, z + v.z, w + v.w);
	}
	inline T length2XYZ() const {
		return x * x + y * y + z * z + w * w;
	}
	inline T lengthXYZ() const {
		return sqrt(length2XYZ());
	}
};

// 3 zlozkovy vektor
template < typename T > class Vec3 {
public:
	T x, y, z;
	Vec3():x(T(0)), y(T(0)), z(T(0)) {

	}
	Vec3(T xx):x(xx), y(xx), z(xx) {

	}
	Vec3(T xx, T yy, T zz):x(xx), y(yy), z(zz) {

	}
	Vec3(const Vec3 < T > &v) : x(v.x), y(v.y), z(v.z) {

	}
	Vec3 & normalize() {
		T nor2 = length2();
		if (nor2 > 0) {
			T invNor = 1 / sqrt(nor2);
			x *= invNor, y *= invNor, z *= invNor;
		}
		return *this;
	}
	inline Vec3 < T > cross(const Vec3 < T > &a) {
		return Vec3 < T > (y * a.z - z * a.y,
							 z * a.x - x * a.z,
							 x * a.y - y * a.x);
	}
	inline Vec3 < T > operator *(const T & f)const {
		return Vec3 < T > (x * f, y * f, z * f);
	}
	inline Vec3 < T > operator *(const Vec3 < T > &v)const {
		return Vec3 < T > (x * v.x, y * v.y, z * v.z);
	}
	inline T dot(const Vec3 < T > &v)const {
		return x * v.x + y * v.y + z * v.z;
	}
	inline Vec3 < T > operator -(const Vec3 < T > &v)const {
		return Vec3 < T > (x - v.x, y - v.y, z - v.z);
	}
	inline Vec3 < T > operator +(const Vec3 < T > &v)const {
		return Vec3 < T > (x + v.x, y + v.y, z + v.z);
	}
	inline Vec3 < T > &operator +=(const Vec3 < T > &v) {
		x += v.x, y += v.y, z += v.z;
		return *this;
	}
	inline Vec3 < T > &operator *=(const Vec3 < T > &v) {
		x *= v.x, y *= v.y, z *= v.z;
		return *this;
	}
	inline Vec3 < T > operator -()const {
		return Vec3 < T > (-x, -y, -z);
	}
	inline T length2() const {
		return x * x + y * y + z * z;
	}
	inline T length() const {
		return sqrt(length2());
	}
	friend std::ostream & operator <<(std::ostream & os, const Vec3 < T > &v) {
		os << "[" << v.x << " " << v.y << " " << v.z << "]";
		return os;
	}
};

//distance estimator pre fraktal
template < typename T > T DistanceEstimator(Vec3 < T > z) {

	const T step = 4;
	Vec3 <T> a1( step, step, step);
	Vec3 <T> a2(-step,-step, step);
	Vec3 <T> a3( step,-step,-step);
	Vec3 <T> a4(-step, step,-step);
	Vec3 <T> c;

	const int Iterations = 15;
	const T	 Scale		= 1.8;
	int		 n			= 0;

	T dist;
	T d;
	while (n < Iterations) {
		c = a1;
		Vec3 <T> tmp;
		tmp = z - a1;
		dist = tmp.length();
		tmp = z - a2;
		d = tmp.length();
		if (d < dist) {
			c = a2;
			dist = d;
		}
		tmp = z - a3;
		d = tmp.length();
		if (d < dist) {
			c = a3;
			dist = d;
		}
		tmp = z - a4;
		d = tmp.length();
		if (d < dist) {
			c = a4;
			dist = d;
		}
		z = z * Scale - c * (Scale - 1.0);
		n++;
	}
	return z.length() * pow(Scale, T(-n));
}

// optimalizovany distance estimator pre fraktal
// vyuziva symetriu generovaneho rekurzivneho tetrahedronu 
template <typename T> T DistanceEstimatorOptimal(Vec3 <T> z) {		
	T pom;
	int n = 0;
	T scale = 1.8;
	const int offset = 8;
	const int iterations = 20; // 20;	

	while (n < iterations) {
		if(z.x + z.y < 0) { // fold1 (zalomenie) xy = - yx
			pom = -z.y; 
			z.y = -z.x;
			z.x = pom;
		}
		if(z.x + z.z < 0) { // fold2 xz = - zx
			pom = -z.z;
			z.z = -z.x;
			z.x = pom;			
		}
		if(z.y + z.z < 0) { // fold3 zy = - yz
			pom = -z.z;
			z.z = -z.y;
			z.y = pom;
		}
		z = z*scale - offset*(scale - 1.0);
		n++;
	}
	return z.length() * pow(scale, -T(n));
}

// upraveny algoritmus od uzivatela Buddhi
// generuje fraktal tvaru kocky s moznostou upravy vysledneho tvaru pomocou:
// kouficienty cx, cy, cz ktore upravuju vysledny tvar fraktulu v x-ovej, y-ovej resp. z-ovej osy
// nutne zvolit vhodny scale (-1.5 - 2.0) pre vizualne pekny vysledok
// u tohoho typu fraktalu pocet iteracii (iterations) odpoveda priamou umerou "velkosti" vysledneho
// objektu 
//http://www.fractalforums.com/3d-fractal-generation/a-mandelbox-distance-estimate-formula/
template <typename T> T DistanceEstimatorCube(Vec3 <T> z) 
{ 
	const int scale = 1.5;
	const int iterations = 20;	
	int i = 0;
	T DEfactor = 0.0;
	T cx = 2.0;
	T cy = 1.0;
	T cz = 2.0;
	T fR2, mR2, r2, minRadius, fixedRadius;
	
	fixedRadius = 1.0;
	fR2 = fixedRadius * fixedRadius;
	minRadius = 0.5; // tato hodnota by mala byt mensia ako hodnota fixedRadius
	mR2 = minRadius * minRadius;

	for(; i < iterations; i++) 
	{	
	DEfactor = scale;
	if (z.x > 1.0)
	z.x = 2.0 - z.x;
	else if (z.x < -1.0) z.x = -2.0 - z.x;
	if (z.y > 1.0)
	z.y = 2.0 - z.y;
	else if (z.y < -1.0) z.y = -2.0 - z.y;
	if (z.z > 1.0)
	z.z = 2.0 - z.z;
	else if (z.z < -1.0) z.z = -2.0 - z.z;

	r2 = z.x*z.x + z.y*z.y + z.z*z.z;

	if (r2 < mR2)
	{
		 z.x = z.x * fR2 / mR2;
		 z.y = z.y * fR2 / mR2;
		 z.z = z.z * fR2 / mR2;
		 DEfactor = DEfactor * fR2 / mR2;
	}
	else if (r2 < fR2)
	{
		 z.x = z.x * fR2 / r2;
		 z.y = z.y * fR2 / r2;
		 z.z = z.z * fR2 / r2;
		 DEfactor *= fR2 / r2;
	}

	z.x = z.x * scale + cx;
	z.y = z.y * scale + cy;
	z.z = z.z * scale + cz;
	//DEfactor *= scale; // pouzity pre velke vygenerovane kocky (cca 3MB, uvedene online, link v dokumentacii)
	DEfactor = DEfactor * abs(scale) + 1.0;	 
	}	
	// pouzit jeden z uvedenych returnov (rozdielne vysledky)
	//return sqrt(z.x*z.x+z.y*z.y+z.z*z.z)/abs(DEfactor); 
	return (sqrt(z.x*z.x+z.y*z.y+z.z*z.z)-abs(scale-1))/abs(DEfactor) - abs(pow(scale, 1-i)); 
}

// pomocna funkcia na "orezanie" hodnoty podla hodnot min, max (hodnota musi lezat v tomto intervale)	
template <typename T> T Clamp(T value, T max, T min)
{
	T result;
	value > max ? result = max : result = value; // value vacsie ako max - vrati sa hodnota max
	value < min ? result = min : result = value; // value mensie ako min - vrati sa hodnota min
	return result;
}

// Mandelbox shader by Rrrola
// Original formula by Tglad
// upraveny zdrojovy kod
// jedna sa o "Continuous Conformal Mandelbrot" fraktal upraveny do distance estimator funkcie 
// - http://www.fractalforums.com/3d-fractal-generation/amazing-fractal
template <typename T> T DistanceEstimatorMandelbrot(Vec3 <T> pos)
{
	const int iters = 20;
	T Scale = 1.5;
	T absScalem1 = abs(Scale - 1.0);
	T AbsScaleRaisedTo1mIters = pow(abs(Scale), float(1-iters));
	T minRadius = 0.25;
	T MinRad2 = minRadius * minRadius;
	T r2;
	T DIST_MULTIPLIER = 1.0;

	Vec4 <T> p(pos.x, pos.y, pos.z, 1.0);
	Vec4 <T> p0(p.x, p.y, p.z, p.w);	// p.w je odhad vzdialenosti
	Vec4 <T> scale(Scale/MinRad2, Scale/MinRad2, Scale/MinRad2, abs(Scale)/MinRad2);
	int i;

	for (i = 0; i < iters; i++) {

	// prehybanie kocky (box folding)
	p.x = Clamp(p.x, -1.0, 1.0) * 2.0 - p.x;
	p.y = Clamp(p.y, -1.0, 1.0) * 2.0 - p.y;
	p.z = Clamp(p.z, -1.0, 1.0) * 2.0 - p.z;

	// prehybanie gule (sphere folding)
	r2 = p.x*p.x + p.y*p.y + p.z*p.z;
	p *= Clamp(std::max(MinRad2/r2, MinRad2), 0.0, 1.0);

	// scale, translacia
	p = p*scale + p0;
	if (r2 > 1000.0) break;
	}
	return ((p.lengthXYZ() - absScalem1) / p.w - AbsScaleRaisedTo1mIters) * DIST_MULTIPLIER;
}

// Simple Distance Estimated 3D fractal - Fragmentarium
// upraveny kod distance estimated Mengerova kocka
template <typename T> T DistanceEstimatorMengerCube(Vec3 <T> z)
{
	Vec3 <T> Offset(1.0, 0.5, 1.0);
	int n = 0;
	const int Iterations = 20;
	T Scale = 3.0;
	T pom;

	while (n < Iterations) 
	{
		z = abs(z);
		if (z.x < z.y)
		{ 
			pom = z.y; 
			z.y = z.x;
			z.x = pom;
		}
		if (z.x < z.z)
		{ 
			pom = z.z;
			z.z = z.x;
			z.x = pom; 
		}
		if (z.y < z.z)
		{ 
			pom = z.z;
			z.z = z.y;
			z.y = pom;
		}
		z = z*Scale - Offset*(Scale-1.0);
		if (z.z < -0.5*Offset.z * (Scale-1.0))	
			z.z += Offset.z*(Scale-1.0);
		n++;
	}
	return z.length() * pow(Scale, -T(n));
	//return abs(z.length() - 0.0) * pow(Scale, float(-n));	
}

//http://blog.hvidtfeldts.net/index.php/2011/08/distance-estimated-3d-fractals-iii-folding-space/
// nejedna sa o fraktal, ale len o generovanie objektov rovnakeho typu 
// (aktualny kod generuje gule)
template <typename T> T DistanceEstimatorObjects(Vec3 <T> z)
{ 
	Vec3 <T> b(0.0, 0.0, 0.0);
	T c = 1.5;

	//vec3 q = mod(p,c)-0.5*c; // opakovanie jednotlivych objektov
	z.x = fmod(z.x, c) - 0.5*c;
	z.y = fmod(z.y, c) - 0.5*c;
	z.z = fmod(z.z, c) - 0.5*c;	

	return z.length() - 0.4; // gula o velkosti -hodnota
}

//absolutna hodnota cisla
template < typename T > T abs(const T v) {
	return v > 0 ? v : -v;
}

//absolutna hodnota 3 zlozkoveho vektoru (abs jednotlivych zloziek)
template < typename T > Vec3 <T> abs(Vec3 <T> v) {
	Vec3 <T> p;
	v.x > 0 ? p.x = v.x : p.x = -v.x;
	v.y > 0 ? p.y = v.y : p.y = -v.y;
	v.z > 0 ? p.z = v.z : p.z = -v.z;
	return p;
}

template < typename T > Vec3 <T> trace(const Vec3 < T > &camera,
	const Vec3 < T > &raydir) {

	const int MaximumRaySteps = 70;
	const T	 MinimumDistance = (T)1e-3;

	Vec3 < T > p; //point of intersection
	T distance;
	T totalDistance = (T)0;
	int steps;
	for (steps = 0; steps < MaximumRaySteps; steps++) {
		p = camera + raydir * totalDistance;
#if 1
	distance = DistanceEstimator(p); // algoritmus generujuci rekurzivny tetrahedron
#elif 0
	distance = DistanceEstimatorOptimal(p); // optimalizovana verzia predchadzajuceho algoritmu
#elif 0
	distance = DistanceEstimatorCube(p); // fraktal tvaru kocky
#elif 0
	distance = DistanceEstimatorObjects(p); // generovanie rovnakych objektov (v tejto forme sa nejedna o fraktal) - gule
#elif 0
	distance = DistanceEstimatorMengerCube(p); // Mengerova kocka - nutne zvolit vhodne parametre v kode + umiestnenie kamery (TODO odladit)
#elif 0
	distance = DistanceEstimatorMandelbrot(p); // Mandelbrot - nutne zvolit vhodne parametre v kode + umiestnenie kamery (TODO odladit)
#endif
		
		totalDistance += distance;
		if (distance < MinimumDistance)
			break;
	}

	if(steps == MaximumRaySteps) {
		//mimo fraktalu
		return Vec3 <T> (0.3, 0.8, 0.5) * abs(raydir.y - raydir.x); //zelenomodra
//		return Vec3 <T>(0.2 * abs(p.x/* - camera.x*/), 0.7 * abs(p.y/* - camera.y*/), 0.5 * abs(p.z));
//		return Vec3 <T> (0.176, 0.913, 0.439) * abs(raydir.y - raydir.x);//x);
//		return Vec3 <T> (0.8, 0.5, 0.3) * abs(raydir.y - raydir.x); //hneda
//		return Vec3 <T> (0.5, 0.3, 0.8) * abs(raydir.y - raydir.x); //modra
	}

	//hlbka sceny
	T depth = (T)1 - T(steps) / T(MaximumRaySteps);

#if 1
	//farba fraktalu je funkciou pozicie priesecniku
	p.normalize();
	p.x = abs(p.x);
	p.y = abs(p.y);
	p.z = abs(p.z);
	return p * depth;
#elif 0
	//farba je hodnota hlbky
	return Vec3 <T> (depth);
#elif 0
	//farba je odtien zelenej nasobeny podla hlbky
	return Vec3 <T> (0.8, 0.9, 0.4) * depth;
#endif
}

//http://www.cprogramming.com/tutorial/3d/quaternions.html
//http://content.gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation
template < typename T >
class Quaternion {
public:
	T x;
	T y;
	T z;
	T w;

	Quaternion() : x(0), y(0), z(0), w(1) {

	}

	Quaternion(const Vec3<T> &vec, T w) : x(vec.x), y(vec.y), z(vec.z), w(w) {

	}

	Quaternion(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {

	}

	Quaternion <T> operator* (const Quaternion <T> &rq) {
		return Quaternion<T>(
			w * rq.x + x * rq.w + y * rq.z - z * rq.y,
			w * rq.y + y * rq.w + z * rq.x - x * rq.z,
			w * rq.z + z * rq.w + x * rq.y - y * rq.x,
			w * rq.w - x * rq.x - y * rq.y - z * rq.z);
	}

	Quaternion<T> getConjugate(void) {
		return Quaternion<T>(-x, -y, -z, w);
	}

	Vec3<T> operator* (const Vec3<T> &vec) {
		Vec3<T>vn(vec);
		//vn.normalize();

		Quaternion <T>vecQuat(vn, (T)0);
		Quaternion <T>resQuat = vecQuat * getConjugate();
		resQuat = *this * resQuat;

		return Vec3<T>(resQuat.x, resQuat.y, resQuat.z);
	}
};

template < typename T >
class Cam {
	#define PIOVER180 (M_PI/180)
public:
	T aspect_ratio;
	T angle;
	T zoom;

	unsigned width;
	unsigned height;
	T invH;
	T invW;

	Vec3 <T> pos;
	Vec3 <T> lookAt;
	Vec3 <T> up;
	Vec3 <T> right;

	Quaternion <T>rot;

	T rotx_val; //TODO
	T roty_val; //TODO

	Cam(unsigned w, unsigned h, unsigned fov) : width(w), height(h) {
		aspect_ratio = w / (T)h;
		invW		 = 1 / (T)w;
		invH		 = 1 / (T)h;
		angle		= tan(M_PI * 0.5 * fov / T(180));
	}

	void GetRay(unsigned x, unsigned y, Vec3 <T> *direction) {
		T norm_x = (2 * ((x + 0.5) * invW) - 1) * angle * aspect_ratio;
		T norm_y = (1 - 2 * ((y + 0.5) * invH)) * angle;

		*direction = rot * Vec3<T>(norm_x, norm_y, zoom);
	}

	inline Vec3 <T> GetLookAt(void) {
		T norm_x = (2 * ((width/2.0 + 0.5) * invW) - 1) * angle * aspect_ratio;	//TODO: optimalizovat!
		T norm_y = (1 - 2 * ((height/2.0 + 0.5) * invH)) * angle;				//TODO: optimalizovat!

		return rot * Vec3<T>(norm_x, norm_y, zoom);
	}
	inline void movex(T xdelta) {
		pos += rot * Vec3<T>(xdelta, 0, 0);
	}
	inline void movey(T ydelta) {
		pos.y -= ydelta;
	}
	inline void movez(T zdelta) {
		pos += rot * Vec3<T>(0, 0, -zdelta);
	}
	void rotx(T xrot) {
		if(xrot != 0)
			rotx_val = xrot;
		Quaternion <T>nrot(Vec3<T>(1, 0, 0), xrot * PIOVER180);
		rot = nrot * rot;
	}
	void roty(T yrot) {
		if(yrot != 0)
			roty_val = yrot;
		Quaternion <T>nrot(Vec3<T>(0, 1, 0), yrot * PIOVER180);
		rot = nrot * rot;
	}

	void WriteInfo(void) {
		printf("%g %g %g %g %g %g\n",
			pos.x,
			pos.y,
			pos.z,
			rotx_val,
			roty_val,
			zoom);
		rotx_val = 0;
		roty_val = 0;
	}
};

template < typename T >
void render(Cam<T> &cam, Vec3 <T> *image) {

	Vec3 <T> *pixel = image;
	Vec3 <T> pos;
	Vec3 <T> direction;

	for(unsigned y = 0; y < cam.height; y++) {
		for(unsigned x = 0; x < cam.width; x++, pixel++) {
			cam.GetRay(x, y, &direction);
			direction.normalize();

			*pixel = trace(cam.pos, direction);
		}
	}
}

template < typename T >
void save_frame(unsigned w, unsigned h, Vec3 <T> *image, unsigned frame) {
	char name[32];
	snprintf(name, sizeof name, "./frame%.5i.ppm", frame);

	//Save result to a PPM image (keep these flags if you compile under Windows)
	std::ofstream ofs(name, std::ios::out | std::ios::binary);
	ofs << "P6\n" << w << " " << h << "\n255\n";
	for (unsigned i = 0; i < w * h; ++i) {
		ofs << (unsigned char)(std::min(T(1), image[i].x) * 255) <<
				 (unsigned char)(std::min(T(1), image[i].y) * 255) <<
				 (unsigned char)(std::min(T(1), image[i].z) * 255);
	}

	ofs.close();
}

//------------------------------------------------------------------------------
//Nacitavanie animacie

struct frame_info {
	double cam_x;
	double cam_y;
	double cam_z;
	double cam_rot_x;
	double cam_rot_y;
	double cam_zoom;
};

int anim_load(const char *file, unsigned *frames, struct frame_info **data) {
	*data = NULL;

	int ret = 0;
	FILE *f = fopen(file, "rb"); 
	if(f == NULL) {
		std::cerr << "fopen(" << file << ")" << std::endl;
		return -1;
	}

	enum ldr_state {
		S_FRAME_CNT,
		S_DATA,
	};

	unsigned frame = 0;
	int state = S_FRAME_CNT;
	char buf[128];
	int c;
	int pos = 0;
	while((c = fgetc(f)) != EOF) {
		if(pos >= (int)sizeof buf) {
			std::cerr << file << ": too long text line" << std::endl;
			ret = -1; //spatny format - prilis dlhy riadok
			break;
		}
		buf[pos] = c;
		if(c != '\n') {
			pos++;
			continue;
		}
		//nacitalo cely riadok
		buf[pos] = 0;
		pos = 0;

		if(buf[0] == 0 || buf[0] == '#')
		//prazdny riadok alebo komentar
			continue;

		if(state == S_FRAME_CNT) {
			//ocakava pocet snimkov
			if(sscanf(buf, "%u", frames) != 1 ||
				*frames == 0) {
				//spatny format
				std::cerr << file << ": invalid frame count" << std::endl;
				ret = -1;
				break;
			}
			state = S_DATA;
			*data = new struct frame_info [*frames];
		} else if(state == S_DATA) {
			if(frame >= *frames) {
				//nesedi pocet snimkov
				std::cerr << file << ": too many frames" << std::endl;
				ret = -1;
				break;
			}

			//nacita data
			struct frame_info *fr_data = &(*data)[frame++];
			if(sscanf(buf, "%lf %lf %lf %lf %lf %lf",
				&fr_data->cam_x,
				&fr_data->cam_y,
				&fr_data->cam_z,
				&fr_data->cam_rot_x,
				&fr_data->cam_rot_y,
				&fr_data->cam_zoom) != 6) {

				std::cerr << file << ": invalid camera data" << std::endl;
				//chybny format
				ret = -1;
				break;
			}
		} else
			//toto by sa nemalo stat, takto o tom
			//aspon budem vediet
			assert(0);
	}

	if(frame != *frames) {
		//nesedi pocet snimkov
		std::cerr << file << ": invalid frame count (does not match)" << std::endl;
		ret = -1;
	}

	if(ret != 0 && *data != NULL)
		//pri chybe uprace pamet
		delete []*data;

	fclose(f);
	return ret;
}

//------------------------------------------------------------------------------
//Multicore

#include <pthread.h>
#include <string.h>

unsigned width	 = 400; 
unsigned height	= 400;
unsigned threads = 1;

unsigned			 anim_frames;
struct frame_info *anim_data;

struct renderer_task {
	Quaternion<double> cam_rot;
	unsigned			 start_frame;
	unsigned			 end_frame;
	int				thread_id;
};

#define CAM_LOOP(frames) \
	for(unsigned start = frame; frame < start + frames; frame++)

#define RENDER_SAVE \
	render<double>(cam, image); \
	save_frame<double>(width, height, image, frame)

void *renderer_proc(void *arg) {
	struct renderer_task *t = (struct renderer_task*)arg;

	pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_lock(&mutex);

	fprintf(stderr, "Thread %p\n"			//vlakno
					"\tstart_frame = %u\n"	 //pociaticny frame
					"\tend_frame	 = %u\n"	 //koncovy frame (cur < end)
					"\tframes		= %u\n",	//pocet frame-ov na renderovanie
		t,
		t->start_frame,
		t->end_frame,
		t->end_frame - t->start_frame);

	pthread_mutex_unlock(&mutex);

	//vytvori framebuffer
	Vec3 <double> *image = new Vec3 <double>[width * height];

	Cam<double> cam(width, height, 30/*fov*/);

	if(t->thread_id != 0) {
		//rotacia kamery pre dane vlakno
		cam.rot = t->cam_rot;
	}
	
	for(unsigned cur_frame = t->start_frame; cur_frame < t->end_frame; cur_frame++) {

		//nastavi kameru podla dat z animacie
		struct frame_info *f = &anim_data[cur_frame];
		cam.pos.x = f->cam_x;
		cam.pos.y = f->cam_y;
		cam.pos.z = f->cam_z;
		cam.zoom	= f->cam_zoom;
		if(f->cam_rot_x != 0) {
			cam.rotx(f->cam_rot_x);
			cam.rotx(0);
		}
		if(f->cam_rot_y != 0) {
			cam.roty(f->cam_rot_y);
			cam.roty(0);
		}

		//vyrenderuje snimok
		render<double>(cam, image);

		//ulozi snimok
		save_frame<double>(width, height, image, cur_frame);
	}

	delete []image;
	return NULL;
}

void calc_cam_rot(Cam <double> *cam, struct renderer_task *t) {
	for(unsigned frame = t->start_frame; frame < t->end_frame; frame++) {
		struct frame_info *f = &anim_data[frame];
		if(f->cam_rot_x != 0) {
			cam->rotx(f->cam_rot_x);
			cam->rotx(0);
		}
		if(f->cam_rot_y != 0) {
			cam->roty(f->cam_rot_y);
			cam->roty(0);
		}
	}
}

int main(int argc, char *argv[]) {
	if(argc != 5) {
		std::cerr << "Usage: ./multicore [threads] [width] [height] [anim_data_file]" << std::endl;
		return 1;
	}
	if(sscanf(argv[1], "%u", &threads) != 1 ||
		sscanf(argv[2], "%u", &width)	 != 1 ||
		sscanf(argv[3], "%u", &height)	!= 1 ||
		threads == 0) {

		std::cerr << "Usage: ./multicore [threads] [width] [height] [anim_data_file]"	<< std::endl;
		return 1;
	}

	if(anim_load(argv[4], &anim_frames, &anim_data) != 0) {
		std::cerr << "Error loading animation file!" << std::endl;
		return 1;
	}

	if(anim_frames / threads == 0) {
		std::cerr << "Too many threads for " << anim_frames << " frames." << std::endl;
		delete []anim_data;
		return 1;
	}

	struct renderer_task main_config;

	if(threads == 1) {
		//pre jedno vlakno priamo zavola renderer
		main_config.start_frame = 0;
		main_config.end_frame	 = anim_frames;

		renderer_proc((void*)&main_config);

		delete []anim_data;
		return 0;
	}

	struct renderer_task *info = new struct renderer_task [threads];
	pthread_t *th_ids			= new pthread_t [threads];

	//pocet snimkov pre kazdy thread
	unsigned frames_per_thread = anim_frames / threads;

	Cam <double>cam(0, 0, 0);

	//rozdeli ulohy a vytvori vlakna
	unsigned thread;
	for(thread = 0; thread < threads; thread++) {
		//pociatocny a koncovy frame
		info[thread].start_frame = frames_per_thread * thread;
		info[thread].end_frame	 = info[thread].start_frame + frames_per_thread;
		info[thread].thread_id	 = thread;

		//rotacia kamery (uplatnene vsetky predosle rotacie kamery)
		if(thread != 0) {
			calc_cam_rot(&cam, &info[thread - 1]);
			info[thread].cam_rot = cam.rot;
		}

		//vytvori vlakno
		int err = pthread_create(&th_ids[thread], NULL, renderer_proc,
			(void*)&info[thread]);
		if(err != 0) {
			std::cerr << "pthread_create(): " << strerror(err) << std::endl;
			break;
		}
	}

	unsigned remaining_frames = anim_frames % threads;
	if(remaining_frames != 0) {
		//zvysili nejake nepridelene snimky, vypocita ich toto vlakno
		main_config.start_frame = info[threads - 1].end_frame;
		main_config.end_frame	 = anim_frames;

		renderer_proc((void*)&main_config);
	}

	//pocka kym vlakna skoncia
	for(thread = 0; thread < threads; thread++) {
		pthread_join(th_ids[thread], NULL);
	}

	//odstrani dynamicky alokovane struktury
	delete []th_ids;
	delete []info;
	delete []anim_data;

	return 0;
}
